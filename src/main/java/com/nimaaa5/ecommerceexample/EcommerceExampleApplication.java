package com.nimaaa5.ecommerceexample;

import com.nimaaa5.ecommerceexample.dao.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
public class EcommerceExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(EcommerceExampleApplication.class, args);
    }

}
