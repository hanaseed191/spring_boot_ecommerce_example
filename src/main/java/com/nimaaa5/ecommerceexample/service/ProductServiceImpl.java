package com.nimaaa5.ecommerceexample.service;


import com.nimaaa5.ecommerceexample.dao.ProductRepository;
import com.nimaaa5.ecommerceexample.entity.Product;
import com.nimaaa5.ecommerceexample.entity.ProductCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> findAllByProductCategory(ProductCategory productCategory, Integer page, Integer size) {
        if (page == null)
            page = 0;
        if (size == null)
            size = 100;
        PageRequest pageRequest = PageRequest.of(page, size);
        return productRepository.findAllByProductCategory(productCategory, pageRequest);
    }

    @Override
    public List<Product> findAll(Integer page, Integer size) {
        if (page == null)
            page = 0;
        if (size == null)
            size = 100;
        PageRequest pageRequest = PageRequest.of(page, size);
        return productRepository.findAll(pageRequest);
    }

    @Override
    public Product findById(long id) {
        Optional<Product> result = productRepository.findById(id);
        if (result.isPresent()) {
            return result.get();
        }
        throw new RuntimeException("Not found!");
    }

    @Override
    public List<Product> findAllProductBySearchedText(String searchedText, Integer page, Integer size) {
        if (page == null)
            page = 0;
        if (size == null)
            size = 100;
        PageRequest pageRequest = PageRequest.of(page, size);
        return productRepository.findByNameContainingIgnoreCase(searchedText, pageRequest);
    }
}
