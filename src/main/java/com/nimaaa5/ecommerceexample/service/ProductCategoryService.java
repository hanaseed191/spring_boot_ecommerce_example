package com.nimaaa5.ecommerceexample.service;


import com.nimaaa5.ecommerceexample.entity.ProductCategory;

import java.util.List;

public interface ProductCategoryService {

    ProductCategory findByName(String name);
    List<ProductCategory> findAllOrderByName();
}
