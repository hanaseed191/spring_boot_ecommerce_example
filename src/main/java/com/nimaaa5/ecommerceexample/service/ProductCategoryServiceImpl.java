package com.nimaaa5.ecommerceexample.service;


import com.nimaaa5.ecommerceexample.dao.ProductCategoryRepository;
import com.nimaaa5.ecommerceexample.entity.ProductCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@CacheConfig(cacheNames = "product_category")
public class ProductCategoryServiceImpl implements ProductCategoryService {

    private final ProductCategoryRepository productCategoryRepository;

    @Autowired
    public ProductCategoryServiceImpl(ProductCategoryRepository productCategoryRepository) {
        this.productCategoryRepository = productCategoryRepository;
    }


    @Override
    public ProductCategory findByName(String name) {
        return productCategoryRepository.findAllByCategoryName(name);
    }

    @Override
    public List<ProductCategory> findAllOrderByName() {
        return productCategoryRepository.findAllByOrderByCategoryName();

    }
}
