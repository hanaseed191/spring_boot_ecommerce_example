package com.nimaaa5.ecommerceexample.service;


import com.nimaaa5.ecommerceexample.entity.Product;
import com.nimaaa5.ecommerceexample.entity.ProductCategory;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductService {
    List<Product> findAllByProductCategory(ProductCategory productCategory, Integer page, Integer size);
    Product findById(long id);
    List<Product> findAll(Integer page, Integer size);
    List<Product> findAllProductBySearchedText(String searchedText, Integer page, Integer size);
}
