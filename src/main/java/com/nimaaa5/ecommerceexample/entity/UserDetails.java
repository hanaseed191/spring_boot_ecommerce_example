package com.nimaaa5.ecommerceexample.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class UserDetails implements org.springframework.security.core.userdetails.UserDetails {

    private String userName;
    private String password;
    private int enabled;
    private List<GrantedAuthority> authorities;

    public UserDetails(User user) {
        this.userName = user.getUserName();
        this.enabled = user.getEnabled();
        String[] s = new String[user.getAuthorities().size()];
        this.authorities = Arrays.stream(user.getAuthorities().toArray(s))
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
        this.password = user.getPassword();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled == 1;
    }
}
