package com.nimaaa5.ecommerceexample.entity;

import com.nimaaa5.ecommerceexample.validator.FieldMatch;
import com.nimaaa5.ecommerceexample.validator.ValidPassword;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "username", unique = true)
    @NotEmpty(message = "is required")
    @Size(min = 3, message = "should be longer than two letters")
    private String userName;

    @NotEmpty(message = "is required")
    @ValidPassword
    @Column(name = "password")
    private String password;

    @Transient
    private String passwordConfirm;

    @NotEmpty(message = "is required")
    @Email(message = "ex: x@x.com")
    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "enabled")
    private int enabled;


    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "authority",
            joinColumns = @JoinColumn(name = "id"))
    @Column(name = "authority")
    private List<String> authorities = new ArrayList<>();


    public User() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<String> authorities) {
        this.authorities = authorities;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
