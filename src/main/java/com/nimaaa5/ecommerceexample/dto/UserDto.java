package com.nimaaa5.ecommerceexample.dto;

import com.nimaaa5.ecommerceexample.validator.FieldMatch;
import com.nimaaa5.ecommerceexample.validator.ValidPassword;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@ToString
@NoArgsConstructor
@FieldMatch
public class UserDto {

    @Email
    @NotNull
    @NotEmpty
    @Size(min = 3, max = 52)
    private String email;

    @NotNull
    @NotEmpty
    @Size(min = 3, max = 52)
    private String username;

    @NotNull
    @NotEmpty
    @ValidPassword
    @Size(min = 6, max = 52)
    private String password;

    @NotNull
    @NotEmpty
    private String passwordConfirm;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
